# Gulp + Handlebars Project Boilerplate
Project boilerplate using Gulp automation & Handlebars components.

## Quick Start

``` bash
# clone git repository
git clone git@github.com:Nick-Shmyrev/gulp-handlebars-boilerplate.git gulp-handlebars-boilerplate

# install dependencies
npm install

# compile sources & start dev. server @ localhost:3000
gulp watch

```

## Gulp Tasks
Includes following Gulp tasks:
- `gulp styles` – CSS minification, concatenation, autoprefixing and sourcemaps
- `gulp styles` – (**Commented out**) SASS compilation, autoprefixing, compression and sourcemaps
- `gulp scripts` – JS ES6 to ES5 transpiling, concatenation, minification and sourcemaps
- `gulp templates` – Precompiles & concatenates Handlebars templates
- `gulp images` – Compresses images
- `gulp devserver` – Starts a livereload development server
- `gulp clean` – Removes all compiled files
- `gulp export` – Compiles and zips-up the project
- `gulp watch` – Compiles all project files & starts development server

## Handlebars components usage
1. Create component files with .hbs extension in */public/_src/components* folder.
2. Register component in */public/_src/js/index.js*: ` regComponent(templateName, tagName);`
- **templateName** – name of the .hbs file containing component.
- **tagName** – name of the custom html tag to be created.
3. Use component as `<tag-name>{"property":"Hello Handlebars!"}</tag-name>` inside your HTML.
4. ...
5. Profit!
