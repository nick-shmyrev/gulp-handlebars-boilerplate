// Custom components constructor
const regComponent = function (templateName, tagName) {
  "use strict";
  const elemArray = Array.from(document.getElementsByTagName(tagName));

  elemArray.forEach((el) => {
    const data = el.textContent;

    // if data is provided, pass it into template.
    if (data) {
      el.outerHTML = templates[templateName](JSON.parse(data));
    } else {
      el.outerHTML = templates[templateName]();
    }
  });
};

// Register components
regComponent('welcome', 'hbs-welcome');

