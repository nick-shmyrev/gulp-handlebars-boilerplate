const gulp = require('gulp');

// Utility plugins
const server = require('gulp-server-livereload');
const plumber = require('gulp-plumber');
const del = require('del');
const zip = require('gulp-zip');

// CSS plugins
const minifyCss = require('gulp-minify-css');
const autoprefixer = require('gulp-autoprefixer');
// const sass = require('gulp-sass');

// JS plugins
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');

// CSS/JS plugins
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');

// Handlebars plugins
const handlebars = require('gulp-handlebars');
const handlebarsLib = require('handlebars');
const declare = require('gulp-declare');
const wrap = require('gulp-wrap');

// Image compression
const imagemin = require('gulp-imagemin');
const imageminPngquant = require('imagemin-pngquant');
const imageminJpegRecompress = require('imagemin-jpeg-recompress');


// File Paths
const PATH = {
  build: {
    js: 'public/js',
    css: 'public/css',
    img: 'public/img'
  },
  src: {
    js: [
      '!public/_src/js/_precompiled/*.js', /* Excludes _precompiled folder */
      'public/_src/js/**/*.js'
    ],
    precompiledJs: 'public/_src/js/_precompiled/*.js',
    css: [
      '!public/_src/css/_precompiled/*.css', /* Excludes _precompiled folder */
      'public/_src/css/index.css',
      'public/_src/css/**/*.css'
    ],
    precompiledCss: 'public/_src/css/_precompiled/*.css',
    sass: 'public/_src/scss/styles.scss',
    templates: 'public/_src/components/**/*.hbs',
    img: 'public/_src/img/**/*.{png,jpeg,jpg,svg,gif}'
  },
  watch: {
    js: 'public/_src/js/**/*.js',
    css: 'public/_src/css/**/*.css',
    sass: 'public/_src/scss/**/*.scss',
    templates: 'public/_src/components/**/*.hbs',
    img: 'public/_src/img/**/*.{png,jpeg,jpg,svg,gif}'
  }
};


// STYLES
gulp.task('styles', () => {
  "use strict";

  gulp.src(PATH.src.css)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(concat('styles.css'))
    .pipe(autoprefixer())
    .pipe(minifyCss())
    .pipe(sourcemaps.write('./_source_maps'))
    .pipe(gulp.dest(PATH.build.css));

  return gulp.src(PATH.src.precompiledCss)
    .pipe(gulp.dest(PATH.build.css));

});


// SASS STYLES
// gulp.task('styles', () => {
//   "use strict";
//
//   gulp.src(PATH.src.sass)
//     .pipe(plumber())
//     .pipe(sourcemaps.init())
//     .pipe(autoprefixer())
//     .pipe(sass({outputStyle: 'compressed'}))
//     .pipe(sourcemaps.write('./_source_maps'))
//     .pipe(gulp.dest(PATH.build.css));
//
//   gulp.src(PATH.src.precompiledCss)
//     .pipe(gulp.dest(PATH.build.css));
// });


// SCRIPTS
gulp.task('scripts', () => {
  "use strict";

  gulp.src(PATH.src.js)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(babel({presets: ['env']}))
    .pipe(concat('scripts.js'))
    .pipe(uglify()) /* minifies the files */
    .pipe(sourcemaps.write('./_source_maps'))
    .pipe(gulp.dest(PATH.build.js));

  return gulp.src(PATH.src.precompiledJs)
    .pipe(gulp.dest(PATH.build.js));
});


// TEMPLATES
gulp.task('templates', () => {
  "use strict";

  return gulp.src(PATH.src.templates)
    .pipe(plumber())
    .pipe(handlebars({
      handlebars: handlebarsLib
    }))
    .pipe(wrap('Handlebars.template(<%= contents %>)'))
    .pipe(declare({
      namespace: 'templates',
      noRedeclare: true
    }))
    .pipe(concat('templates.js'))
    .pipe(gulp.dest(PATH.build.js));
});


// IMAGES
gulp.task('images', () => {
  "use strict";

  return gulp.src(PATH.src.img)
    .pipe(imagemin(
      [
        imagemin.gifsicle(),
        imagemin.jpegtran(),
        imagemin.optipng(),
        imagemin.svgo(),
        imageminPngquant(),
        imageminJpegRecompress()
      ]
    ))
    .pipe(gulp.dest(PATH.build.img));
});


// SERVER
gulp.task('devserver', () => {
  "use strict";

  return gulp.src('public/.')
    .pipe(
      server({
        livereload: true,
        directoryListing: false,
        open: true,
        port: 3000,
        defaultFile: 'index.html'
      })
    );
});

// DEFAULT
gulp.task('default', ['clean', 'scripts', 'styles', 'templates', 'images']);


// CLEAN-UP COMPILED FILES
gulp.task('clean', () => {
  "use strict";

  return del.sync([
    PATH.build.css,
    PATH.build.js,
    PATH.build.img
  ]);
});


// ZIP-UP THE PROJECT
gulp.task('export', ['default'], () => {
  "use strict";

  return gulp.src([
    '!public/_src/**/*', /* Exclude source files folder */
    'public/**/*'
  ])
    .pipe(zip('website.zip'))
    .pipe(gulp.dest('./'));
});


// WATCH
gulp.task('watch', ['default', 'devserver'], () => {
  "use strict";

  gulp.watch(PATH.watch.js, ['scripts']);
  gulp.watch(PATH.watch.css, ['styles']);
  // gulp.watch(PATH.watch.sass, ['styles']);
  gulp.watch(PATH.watch.templates, ['templates']);
});